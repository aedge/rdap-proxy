from typing import Final

from asyncwhois import aio_whois_domain
from asyncwhois.errors import NotFoundError, WhoIsError
from fastapi import FastAPI
from fastapi.exceptions import HTTPException
from fastapi.middleware.cors import CORSMiddleware
from starlette import status
from tldextract import extract as extract_tld  # pyright: ignore

from app.models.rdap import DomainResponse, Notice
from app.transforms import domain_response_from_parser_output

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# List of RDAP levels that we implement.
RDAP_CONFORMANCE: Final = (
    "rdap_level_0",
    "icann_rdap_technical_implementation_guide_0",
    "icann_rdap_response_profile_0",
)


SUMMARY_REMARK: Final = Notice(
    description=[
        "Summary data only.",
        "This RDAP proxy returns limited data.",
    ],
    title="Incomplete Data",
    type="object truncated due to unexplainable reasons",
)

NYPA_NOTICE: Final = Notice(
    title="About this service",
    description=[
        "Please note that this RDAP proxy is not your personal army.",
        "",
        "Engage at your own discretion.",
    ],
)


@app.get("/domain/{domain_name}", response_model=DomainResponse, response_model_exclude_unset=True)
async def get_domain(domain_name: str):
    extract_result = extract_tld(domain_name)
    normalized_domain = extract_result.registered_domain.lower()
    # if extract_result.suffix in ("dev",):
    #     raise HTTPException(
    #         status.HTTP_307_TEMPORARY_REDIRECT,
    #         "This TLD supports RDAP natively",
    #         {
    #             "Location": f"https://rdap.org/domain/{normalized_domain}",
    #         },
    #     )

    if "." not in normalized_domain:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            "Domain name is invalid",
        )

    try:
        response = await aio_whois_domain(normalized_domain)
    except NotFoundError as ex:
        raise HTTPException(status.HTTP_404_NOT_FOUND, str(ex)) from ex
    except WhoIsError as ex:
        raise HTTPException(status.HTTP_503_SERVICE_UNAVAILABLE, str(ex)) from ex

    return domain_response_from_parser_output(response.parser_output, response.query_output)
