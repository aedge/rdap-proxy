

# List of RDAP levels that we implement.
from typing import Final

from app.models.rdap import Notice

RDAP_CONFORMANCE: Final = (
    "rdap_level_0",
    "icann_rdap_technical_implementation_guide_0",
    "icann_rdap_response_profile_0",
)


SUMMARY_REMARK: Final = Notice(
    description=[
        "Summary data only.",
        "This RDAP proxy returns limited data.",
    ],
    title="Incomplete Data",
    type="object truncated due to unexplainable reasons",
)

NYPA_NOTICE: Final = Notice(
    title="About this service",
    description=[
        "Please note that this RDAP proxy is not your personal army.",
        "",
        "Engage at your own discretion.",
    ],
)
