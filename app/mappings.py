from types import MappingProxyType

from app.models.rdap import Status

EPP_TO_RDAP_STATUS = MappingProxyType({
    "addPeriod": Status.add_period,
    "autoRenewPeriod": Status.auto_renew_period,
    "inactive": Status.inactive,
    "ok": Status.active,
    "pendingCreate": Status.pending_create,
    "pendingDelete": Status.pending_delete,
    "pendingRenew": Status.pending_renew,
    "pendingRestore": Status.pending_restore,
    "pendingTransfer": Status.pending_transfer,
    "pendingUpdate": Status.pending_update,
    "redemptionPeriod": Status.redemption_period,
    "renewPeriod": Status.renew_period,
    "serverDeleteProhibited": Status.server_delete_prohibited,
    "serverHold": Status.server_hold,
    "serverRenewProhibited": Status.server_renew_prohibited,
    "serverTransferProhibited": Status.server_transfer_prohibited,
    "serverUpdateProhibited": Status.server_update_prohibited,
    "transferPeriod": Status.transfer_period,
    "clientDeleteProhibited": Status.client_delete_prohibited,
    "clientHold": Status.client_hold,
    "clientRenewProhibited": Status.client_renew_prohibited,
    "clientTransferProhibited": Status.client_transfer_prohibited,
    "clientUpdateProhibited": Status.client_update_prohibited,
})
