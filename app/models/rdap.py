from __future__ import annotations

import json
from datetime import datetime
from enum import Enum
from inspect import cleandoc
from typing import Any, Dict, List, Literal, Optional

from pydantic import BaseModel, Extra, Field, root_validator
from pydantic.schema import schema


class Link(BaseModel):
    """
    The "links" array is found in data structures to signify links to other
    resources on the Internet. The relationship of these links is defined by
    the IANA registry described by [RFC 8288].

    NB: value is required by spec, but some registries omit it?
    (Perhaps should set it from href)

    [RFC 8288]: https://datatracker.ietf.org/doc/html/rfc8288
    """

    class Config(object):
        extra = Extra.forbid

    value: Optional[str]  # noqa: WPS110
    rel: str = ...
    href: str = ...
    hreflang: Optional[List[str]]
    title: Optional[str]
    media: Optional[str]
    type: Optional[str]


class Notice(BaseModel):
    """
    The "notices" and "remarks" data structures take the same form. The notices
    structure denotes information about the service providing RDAP information
    and/or information about the entire response, whereas the remarks structure
    denotes information about the object class that contains it (see Section 5
    regarding object classes).

    @link https://datatracker.ietf.org/doc/html/rfc9083#section-4.3
    """

    title: Optional[str]
    description: List[str]
    links: Optional[List[Link]]
    type: Optional[str]


class RdapLang(BaseModel):
    """
    This data structure consists solely of a name/value pair, where the name is
    "lang" and the value is a string containing a language identifier as
    described in [RFC5646].

    @link https://datatracker.ietf.org/doc/html/rfc9083#section-4.4
    """

    __root__: str = ...


class EventAction(Enum):
    """
    - registration – The object instance was initially registered.
    - reregistration – The object instance was registered subsequently to
      initial registration.
    - last changed – An action noting when the information in the object
      instance was last changed.
    - expiration – The object instance has been removed or will be removed at a
        predetermined date and time from the registry.
    - deletion – The object instance was removed from the registry at a point
      in time that was not predetermined.
    - reinstantiation – The object instance was reregistered after having been
      removed from the registry.
    - transfer – The object instance was transferred from one registrar to
      another.
    - locked – The object instance was locked (see the "locked" status).
    - unlocked – The object instance was unlocked (see the "locked" status).

    @link https://www.iana.org/assignments/rdap-json-values/rdap-json-values.xhtml
    """

    registration = "registration"
    reregistration = "reregistration"
    last_changed = "last changed"
    expiration = "expiration"
    deletion = "deletion"
    reinstantiation = "reinstantiation"
    transfer = "transfer"
    locked = "locked"
    unlocked = "unlocked"


class Event(BaseModel):
    """
    This data structure represents events that have occurred on an instance of
    an object class.
    """

    class Config(object):
        extra = Extra.forbid

    event_action: EventAction = Field(
        ...,
        alias="eventAction",
        description="the reason for the event",
    )
    event_actor: Optional[str] = Field(
        None,
        alias="eventActor",
        description="the actor responsible for the event",
    )
    event_date: datetime = Field(
        ...,
        alias="eventDate",
        description="the time and date the event occurred",
    )
    links: Optional[List[object]]


class Status(Enum):
    """
    This data structure, named "status", is an array of strings indicating the
    state of a registered object (see Section  for a list of values).

    @link https://datatracker.ietf.org/doc/html/rfc9083#section-4.6
    @link https://datatracker.ietf.org/doc/html/rfc9083#section-10.2.2
    @link https://www.iana.org/assignments/rdap-json-values/rdap-json-values.xhtml
    @link https://datatracker.ietf.org/doc/html/rfc8056
    """

    validated = "validated"
    renew_prohibited = "renew prohibited"
    update_prohibited = "update prohibited"
    transfer_prohibited = "transfer prohibited"
    delete_prohibited = "delete prohibited"
    proxy = "proxy"
    private = "private"
    removed = "removed"
    obscured = "obscured"
    associated = "associated"
    active = "active"
    inactive = "inactive"
    locked = "locked"
    pending_create = "pending create"
    pending_renew = "pending renew"
    pending_transfer = "pending transfer"
    pending_update = "pending update"
    pending_delete = "pending delete"
    add_period = "add period"
    auto_renew_period = "auto renew period"
    client_delete_prohibited = "client delete prohibited"
    client_hold = "client hold"
    client_renew_prohibited = "client renew prohibited"
    client_transfer_prohibited = "client transfer prohibited"
    client_update_prohibited = "client update prohibited"
    pending_restore = "pending restore"
    redemption_period = "redemption period"
    renew_period = "renew period"
    server_delete_prohibited = "server delete prohibited"
    server_renew_prohibited = "server renew prohibited"
    server_transfer_prohibited = "server transfer prohibited"
    server_update_prohibited = "server update prohibited"
    server_hold = "server hold"
    transfer_period = "transfer period"


class PublicId(BaseModel):
    class Config(object):
        extra = Extra.forbid

    type: str = ...
    identifier: str = ...


class Role(Enum):
    """
    @link https://www.iana.org/assignments/rdap-json-values/rdap-json-values.xhtml
    """
    registrant = "registrant"
    technical = "technical"
    administrative = "administrative"
    abuse = "abuse"
    billing = "billing"
    registrar = "registrar"
    reseller = "reseller"
    sponsor = "sponsor"
    proxy = "proxy"
    notifications = "notifications"
    noc = "noc"


class Entity(BaseModel):
    """
    The entity object class appears throughout this document and is an
    appropriate response for the /entity/XXXX query defined in "Registration
    Data Access Protocol (RDAP) Query Format" [RFC 9082]. This object class
    represents the information of organizations, corporations, governments,
    non-profits, clubs, individual persons, and informal groups of people.  All
    of these representations are so similar that it is best to represent them
    in JSON [RFC 8259] with one construct, the entity object class, to aid in
    the reuse of code by implementers.

    The entity object class uses jCard [RFC 7095] to represent contact
    information, such as postal addresses, email addresses, phone numbers and
    names of organizations and individuals.  Many of the types of information
    that can be represented with jCard have little or no use in RDAP, such as
    birthdays, anniversaries, and gender. The entity object is served by both
    RIRs and DNRs.

    [RFC 7095]: https://datatracker.ietf.org/doc/html/rfc7095
    [RFC 8259]: https://datatracker.ietf.org/doc/html/rfc8259
    [RFC 9082]: https://datatracker.ietf.org/doc/html/rfc9082

    @link https://datatracker.ietf.org/doc/html/rfc9083#section-5.1
    """

    class Config(object):
        extra = Extra.forbid

    object_class_name: Literal["entity"] = Field(..., alias="objectClassName")
    handle: Optional[str] = Field(
        None,
        description="A string representing a registry-unique identifier of the entity.",
    )
    vcard_array: Optional[List[object]] = Field(None, alias="vcardArray")
    roles: Optional[List[Role]]
    public_ids: Optional[List[PublicId]] = Field(None, alias="publicIds")
    entities: Optional[List[Entity]]
    remarks: Optional[List[Notice]]
    links: Optional[List[Link]]
    events: Optional[List[Event]]
    as_event_actor: Optional[List[Event]] = Field(None, alias="asEventActor")
    status: Optional[List[Status]]
    port43: Optional[str]
    networks: Optional[List[object]]
    autnums: Optional[List[object]]

    @root_validator(pre=True)
    def set_object_class_name(cls, values):  # noqa: N805, WPS615
        if "objectClassName" not in values:
            values["objectClassName"] = "entity"

        return values


class IpAddresses(BaseModel):
    class Config(object):
        extra = Extra.forbid

    v4: List[str] = ...
    v6: List[str] = ...


class Nameserver(BaseModel):
    """
    The nameserver object class represents information regarding DNS
    nameservers used in both forward and reverse DNS.  RIRs and some DNRs
    register or expose nameserver information as an attribute of a domain name,
    while other DNRs model nameservers as "first class objects". Please note
    that some of the examples in this section include lines that have been
    wrapped for reading clarity.s

    @link https://datatracker.ietf.org/doc/html/rfc9083#section-5.2
    """

    class Config(object):
        extra = Extra.forbid

    object_class_name: Literal["nameserver"] = Field(..., alias="objectClassName")
    handle: Optional[str] = Field(
        None,
        description="A string representing a registry-unique identifier of the nameserver.",
    )
    ldh_name: Optional[str] = Field(None, alias="ldhName")
    unicode_name: Optional[str] = Field(None, alias="unicodeName")
    ip_addresses: Optional[IpAddresses] = Field(None, alias="ipAddresses")
    entities: Optional[List[Entity]]
    status: Optional[List[Status]]
    remarks: Optional[List[Notice]]
    links: Optional[List[Link]]
    port43: Optional[str] = Field(None, alias="port43")
    events: Optional[List[Event]]

    @root_validator(pre=True)
    def set_object_class_name(cls, values):  # noqa: N805, WPS615
        if "objectClassName" not in values:
            values["objectClassName"] = "nameserver"

        return values


class DsDatum(BaseModel):
    class Config(object):
        extra = Extra.forbid

    key_tag: float = Field(
        ...,
        description=cleandoc("""
        An integer as specified by the key tag field of a DNS DS record as
        specified by [RFC 4034](https://datatracker.ietf.org/doc/html/rfc4034)
        in presentation format.
        """),
        alias="keyTag",
    )
    algorithm: float = Field(
        ...,
        description=cleandoc("""
        An integer as specified by the algorithm field of a DNS DS record as
        described by [RFC 4034](https://datatracker.ietf.org/doc/html/rfc4034)
        in presentation format.
        """),
    )
    digest: str = Field(
        ...,
        description=cleandoc("""
        A string as specified by the digest field of a DNS DS record as
        specified by [RFC 4034](https://datatracker.ietf.org/doc/html/rfc4034)
        in presentation format.
        """),
    )
    digest_type: float = Field(
        ...,
        description=cleandoc("""
        An integer as specified by the digest type field of a DNS DS record as
        specified by [RFC 4034](https://datatracker.ietf.org/doc/html/rfc4034)
        in presentation format.
        """),
        alias="digestType",
    )
    events: List[Event] = ...
    links: List[Link] = ...


class KeyDatum(BaseModel):
    class Config(object):
        extra = Extra.forbid

    flags: float = Field(
        ...,
        description=cleandoc("""
        An integer representing the flags field value in the DNSKEY record [RFC
        4034](https://datatracker.ietf.org/doc/html/rfc4034) in presentation
        format.
        """),
    )
    protocol: float = Field(
        ...,
        description=cleandoc("""
        An integer representation of the protocol field value of the DNSKEY
        record [RFC 4034](https://datatracker.ietf.org/doc/html/rfc4034) in
        presentation format.
        """),
    )
    public_key: str = Field(
        ...,
        description=cleandoc("""
        A string representation of the public key in the DNSKEY record [RFC
        4034](https://datatracker.ietf.org/doc/html/rfc4034) in presentation
        format.
        """),
        alias="publicKey",
    )
    algorithm: float = Field(
        ...,
        description=cleandoc("""
        An integer as specified by the algorithm field of a DNSKEY record as
        specified by [RFC 4034](https://datatracker.ietf.org/doc/html/rfc4034)
        in presentation format.
        """),
    )
    events: List[Event] = Field(...)
    links: List[Link] = Field(...)


class SecureDnsConfig(BaseModel):
    class Config(object):
        extra = Extra.forbid

    zone_signed: bool = Field(
        ...,
        description="boolean true if the zone has been signed, false otherwise.",
        alias="zoneSigned",
    )
    delegation_signed: bool = Field(
        ...,
        description="boolean true if there are DS records in the parent, false otherwise.",
        alias="delegationSigned",
    )
    max_sig_life: float = Field(
        ...,
        description="an integer representing the signature lifetime in seconds to be used when creating the RRSIG DS record in the parent zone [RFC 5910](https://datatracker.ietf.org/doc/html/rfc5910).",
        alias="maxSigLife",
    )
    ds_data: List[DsDatum] = Field(..., alias="dsData")
    key_data: List[KeyDatum] = Field(..., alias="keyData")


class Domain(BaseModel):
    class Config(object):
        extra = Extra.forbid

    object_class_name: Literal["domain"] = Field(..., alias="objectClassName")
    handle: Optional[str] = Field(
        None,
        description="A string representing a registry-unique identifier of the domain.",
    )
    ldh_name: Optional[str] = Field(None, alias="ldhName")
    unicode_name: Optional[str] = Field(None, alias="unicodeName")
    variants: Optional[List[Dict[str, Any]]]
    nameservers: Optional[List[Nameserver]] = Field(
        None,
    )
    secure_dns: Optional[SecureDnsConfig] = Field(None, alias="secureDNS")
    entities: Optional[List[Entity]]
    status: Optional[List[Status]]
    public_ids: Optional[List[PublicId]] = Field(None, alias="publicIds")
    remarks: Optional[List[Notice]]
    links: Optional[List[Link]]
    port43: Optional[str] = Field(None, alias="port43")
    events: Optional[List[Event]]
    network: Optional[Dict[str, Any]]

    @root_validator(pre=True)
    def set_object_class_name(cls, values):  # noqa: N805, WPS615
        if "objectClassName" not in values:
            values["objectClassName"] = "domain"

        return values


Entity.update_forward_refs()


class ResponseRootMixin(BaseModel):
    rdap_conformance: List[str] = Field(..., alias="rdapConformance")
    notices: Optional[List[Notice]]


class DomainResponse(ResponseRootMixin, Domain):
    pass


if __name__ == "__main__":
    print(  # noqa: WPS421
        json.dumps(
            schema([], title="RDAP schema"),
            indent=2,
        ),
    )
