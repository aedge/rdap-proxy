import json
from csv import DictReader

import httpx

from app.keyize import keyize

IANA_LIST_URL = "https://www.iana.org/assignments/registrar-ids/registrar-ids-1.csv"
INTERNIC_LIST_URL = "https://www.internic.net/registrars.csv"

resp = httpx.get(IANA_LIST_URL)
iana_raw = DictReader(resp.text.splitlines())
# resp = httpx.get(INTERNIC_LIST_URL)
# internic_raw = DictReader(resp.text.splitlines())


registrars = {
    keyize(row["Registrar Name"]): {
        "name": row["Registrar Name"],
        "id": row["ID"],
    }
    for row in iana_raw
}

print(json.dumps(registrars, indent=4))
