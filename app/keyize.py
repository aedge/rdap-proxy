from typing import Optional

KEY_SUFFIXES = ("ltd", "limited", "llc", "corporation", "inc", "gmbh", "pty")


def keyize(string: Optional[str]) -> Optional[str]:
    if string is None:
        return None

    string = "".join(ch if ch.isalnum() else "" for ch in string.lower())

    for suffix in KEY_SUFFIXES:
        if string.endswith(suffix):
            string = string[:-len(suffix)]

    return string
