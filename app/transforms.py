from pprint import pformat
from typing import Final

from app.keyize import keyize
from app.mappings import EPP_TO_RDAP_STATUS
from app.models.rdap import (
    DomainResponse,
    Entity,
    Event,
    EventAction,
    Link,
    Notice,
    PublicId,
    Role,
    Status,
)
from app.notices import NYPA_NOTICE, RDAP_CONFORMANCE, SUMMARY_REMARK
from app.registrars import REGISTRAR_IDS

DO_DEBUG: Final = True


def get_statuses_from_epp(epp_statuses: list[str]) -> list[Status]:
    return [
        EPP_TO_RDAP_STATUS[status]
        for status in (epp_code.split(" ", 1)[0] for epp_code in epp_statuses)
        if status in EPP_TO_RDAP_STATUS
    ]


def get_registrar(parser_output) -> Entity | None:
    registrar = REGISTRAR_IDS.get(keyize(parser_output.get("registrar")))
    if registrar is None:
        return None

    return Entity(
        roles=[Role.registrar],
        status=[Status.active],
        handle=registrar["id"],
        publicIds=[PublicId(identifier=registrar["id"], type="IANA Registrar ID")],
        links=[
            Link(
                rel="self",
                href=f"https://registrars.rdap.org/entity/{registrar['id']}-iana",
                type="application/rdap+json",
            ),
        ],
        vcardArray=[
            "vcard",
            [
                ["version", {}, "text", "4.0"],
                ["fn", {}, "text", parser_output["registrar"]],
            ],
        ],
        remarks=[SUMMARY_REMARK],
    )


def get_events(parser_output) -> list[Event]:
    registration_date = parser_output.get("created")
    last_changed_date = parser_output.get("updated")
    expiration_date = parser_output.get("expires")

    events = []
    if registration_date is not None:
        events.append(
            Event(
                eventAction=EventAction.registration,
                eventDate=registration_date,
            ),
        )
    if last_changed_date is not None:
        events.append(
            Event(
                eventAction=EventAction.last_changed,
                eventDate=last_changed_date,
            ),
        )
    if expiration_date is not None:
        events.append(
            Event(
                eventAction=EventAction.expiration,
                eventDate=expiration_date,
            ),
        )

    return events


def domain_response_from_parser_output(parser_output: dict, query_output: str = None) -> DomainResponse:
    registrar = get_registrar(parser_output)
    events = get_events(parser_output)

    return DomainResponse(
        rdapConformance=RDAP_CONFORMANCE,
        ldhName=parser_output["domain_name"],
        handle=parser_output["domain_name"],
        entities=[] if registrar is None else [registrar],
        events=events,
        status=get_statuses_from_epp(parser_output["status"]),
        remarks=[SUMMARY_REMARK],
        notices=[
            NYPA_NOTICE,
            *([
                Notice(
                    title="Raw whois output",
                    description=query_output.splitlines(),
                ),
                Notice(
                    title="Raw parser output",
                    description=pformat(parser_output).splitlines(),
                ),
            ] if DO_DEBUG else []),
        ],
    )
